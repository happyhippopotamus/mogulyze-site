@extends('layouts.app-splash')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{-- @include('partials.page-header') --}}
    <section style="margin:auto; max-width:600px;">
      @include('partials.content-page')
    </section>
  @endwhile
@endsection
